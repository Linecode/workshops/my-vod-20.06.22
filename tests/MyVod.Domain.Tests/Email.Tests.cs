using System;
using FluentAssertions;
using Xunit;

namespace MyVod.Domain.Tests;

public class EmailTests
{
    [Fact]
    public void Email_Should_Be_Creatable()
    {
        // Arrange
        var email = "test@test.pl";
        // Act
        var result = Email.Create(email);
        // Assert
        result.Should().NotBeNull();
    }

    [Theory]
    [InlineData("")]
    [InlineData("dsadasdsadasdasdasdas")]
    public void Email_Should_Not_Be_Created(string value)
    {
        var result = Record.Exception(() => { Email.Create(value); });

        result.Should().BeOfType<ArgumentException>();
    }
}