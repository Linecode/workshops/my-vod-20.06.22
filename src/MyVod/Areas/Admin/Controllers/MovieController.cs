using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyVod.Areas.Admin.ViewModels;
using MyVod.Infrastructure.Models;
using MyVod.Services;

namespace MyVod.Areas.Admin.Controllers;

[Authorize]
[Area("admin")]
public class MovieController : Controller
{
    private readonly IMovieService _movieService;
    private readonly IPersonService _personService;
    private readonly IGenreService _genreService;

    public MovieController(IMovieService movieService, IPersonService personService, IGenreService genreService)
    {
        _movieService = movieService;
        _personService = personService;
        _genreService = genreService;
    }

    public IActionResult Index()
    {
        var movies = _movieService.Get();
        
        return View("Index", movies.AsEnumerable());
    }
    
    [HttpGet]
    [ActionName("create")]
    public IActionResult CreateView()
    {
        var persons = _personService.Get();
        var genres = _genreService.Get();

        var moviePageModel = new MoviePageModel
        {
            Persons = persons.AsEnumerable().Select(x => new SelectListItem($"{x.FirstName} {x.LastName}", x.Id.ToString())).ToList(),
            Genres = genres.AsEnumerable().Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList(),
        };
        
        return View("Create", moviePageModel);
    }

    [HttpPost]
    [ActionName("create")]
    public async Task<IActionResult> Create(MoviePageModel movie)
    {
        movie.Current.DirectorId = movie.Director;
        movie.Current.GenreId = movie.Genre;
            
        await _movieService.Create(movie.Current);
        
        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet("{area}/{controller}/{action}/{id:guid}/delete")]
    [ActionName("delete")]
    public async Task<IActionResult> Delete(Guid id)
    {
        if (ModelState.IsValid)
        {
            await _movieService.Delete(id);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }
    
    [HttpGet("{area}/{controller}/{action}/{id:guid}")]
    [ActionName("edit")]
    public async Task<IActionResult> EditView(Guid id)
    {
        var movie = await _movieService.Get(id);

        if (movie is null)
            return NotFound();
        
        var persons = _personService.Get();
        var genres = _genreService.Get();

        var moviePageModel = new MoviePageModel
        {
            Persons = persons.AsEnumerable().Select(x => new SelectListItem($"{x.FirstName} {x.LastName}", x.Id.ToString())).ToList(),
            Genres = genres.AsEnumerable().Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToList(),
            Current = movie
        };

        return View("Edit", moviePageModel);
    }

    [HttpPost("{area}/{controller}/{action}/{id:guid}")]
    [ActionName("edit")]
    public async Task<IActionResult> Edit(Guid id, MoviePageModel movie)
    {
        if (id != movie.Current.Id)
            return BadRequest();
        
        movie.Current.DirectorId = movie.Director;
        movie.Current.GenreId = movie.Genre;
        
        await _movieService.Update(movie.Current);
        
        return RedirectToAction(nameof(Index));
    }
}