using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.MovieCatalog.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.MoviesCatalog;

public class MoviesRepository : IMoviesRepository
{
    private readonly MovieDbContext _context;

    public Common.BuildingBlocks.EFCore.IUnitOfWork UnitOfWork => _context;

    public MoviesRepository(MovieDbContext context)
    {
        _context = context;
    }
    
    public Task Add(Movie movie)
    {
        throw new NotImplementedException();
    }

    public Task<Movie?> Get(MovieId id)
    {
        return _context.Movies
            .Include(x => x.Director)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<IEnumerable<Movie?>> Get(Specification<Movie> spec)
    {
        return await _context.Movies.Where(spec.ToExpression()!)
            .AsNoTracking()
            .Include(x => x.Director)
            .ToListAsync();
    }

    public void Update(Movie movie)
    {
        _context.Movies.Update(movie);
    }
}