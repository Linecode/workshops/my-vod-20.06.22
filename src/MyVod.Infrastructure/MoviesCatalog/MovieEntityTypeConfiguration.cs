using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.MoviesCatalog;

public class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
{
    public void Configure(EntityTypeBuilder<Movie> builder)
    {
        builder.ToTable("Movies");

        builder.HasKey(x => x.Id);
        
        builder.Property(x => x.Id)
            .HasConversion(id => id.Value, s => new MovieId(s));

        builder.Ignore(x => x.Timestamp);

        builder.Property(x => x.Cover)
            .HasConversion<string>();

        builder.Property(x => x.Trailer)
            .HasConversion<string>();

        builder.OwnsOne(x => x.Description, d =>
        {
            d.Property(x => x.Value)
                .HasColumnName("Description");
        });

        builder.Property(x => x.Status);

        builder.OwnsOne(x => x.Title, t =>
        {
            t.Property(x => x.English)
                .HasColumnName("EnglishTitle");

            t.Property(x => x.Original)
                .HasColumnName("Title");
        });

        builder.OwnsOne(x => x.MetaData, md =>
        {
            md.Property(x => x.Origin)
                .HasColumnName("Origin");

            md.Property(x => x.ReleasedAt)
                .HasColumnName("ReleasedAt");
        });
        
        builder.Ignore(x => x.DomainEvents);

        builder.HasOne(x => x.Director);
    }
}