using Microsoft.Extensions.DependencyInjection;
using MyVod.Domain.Sales.Application.Ports;

namespace MyVod.StripePaymentGateway;

public class StripeConfiguration
{
    public string SuccessUrl { get; set; }
    public string ErrorUrl { get; set; }
    public string ApiKey { get; set; }
}

public static class ServiceExtension
{
    public static IServiceCollection AddMyVodStripe(this IServiceCollection services, Action<StripeConfiguration> conf)
    {
        var config = new StripeConfiguration();
        
        conf.Invoke(config);

        services.AddSingleton(_ => config);

        services.AddTransient<IPaymentGateway, StripePaymentGateway.StripePaymentService>();
        
        return services;
    }
}
