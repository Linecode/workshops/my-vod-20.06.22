using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MovieCatalog.Domain;

public class Title : ValueObject<Title>
{
    public string Original { get; private set; }
    public string English { get; private set; }

    [Obsolete("Only For EF", true)]
    private Title()
    { }
    
    public Title(string originalTitle, string englishTitle)
    {
        Ensure.That(originalTitle).HasLengthBetween(2, 100);
        Ensure.That(englishTitle).HasLengthBetween(2, 100);
        
        Original = originalTitle;
        English = englishTitle;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Original;
        yield return English;
    }
}