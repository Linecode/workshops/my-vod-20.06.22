using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MovieCatalog.Domain;

public sealed class Movie : Entity<MovieId>, AggregateRoot<MovieId>
{
    public Title Title { get; private set; } = null!;
    public Description Description { get; private set; } = null!;

    public MetaData MetaData { get; private set; } = null!;

    public Uri Cover { get; private set; } = null!;
    public Uri Trailer { get; private set; } = null!;

    public Director Director { get; private set; } = null!;

    public MovieStatus Status { get; private set; } = MovieStatus.UnPublished;

    public byte[] Timestamp { get; private set; } = null!;

    // public RegionIdentifier RegionIdentifier { get; private set; } = null!;

    // public Money Price { get; private set; } = null!;

    [Obsolete("Only For EF", true)]
    private Movie()
    { }

    internal Movie(MovieId id)
    {
        Id = id;
    }

    public Movie(Title title, Description description)
    {
        Title = title;
        Description = description;
        
        Id = MovieId.New();
    }

    public void AddMetaData(MetaData metaData)
    {
        MetaData = metaData;
    }

    public void DefineMedia(Uri cover, Uri trailer)
    {
        Ensure.That(cover).IsNotNull();
        Ensure.That(trailer).IsNotNull();
        
        Cover = cover;
        Trailer = trailer;
    }

    public void Publish()
    {
        // var result = policy.Publish(this);

        // if (result.IsSuccessful)
        // {
        //     Status = MovieStatus.Published;
        //     RegionIdentifier = regionIdentifier;
        // }

        Status = MovieStatus.Published;
    }

    public void UnPublish()
    {
        Status = MovieStatus.UnPublished;
    }

    // public bool HasPrice => Price != null!;

    public enum MovieStatus
    {
        UnPublished,
        Published
    }
}