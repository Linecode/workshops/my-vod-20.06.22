using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MovieCatalog.Domain;

public class MetaData : ValueObject<MetaData>
{
    public string Origin { get; private set; }
    public DateTime? ReleasedAt { get; private set; }

    public MetaData(string origin, DateTime? releasedAt)
    {
        Ensure.That(origin).HasLengthBetween(2, 500);
        
        Origin = origin;
        ReleasedAt = releasedAt;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Origin;
        yield return ReleasedAt;
    }
}