using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MovieCatalog.Domain;

public class Director : Entity<DirectorId>
{
    public string FirstName { get; private set; }
    public string LastName { get; private set; }

    [Obsolete("Only for EF Core", true)]
    private Director()
    {
    }
    
    public Director(string firstName, string lastName)
    {
        Ensure.That(firstName).HasLengthBetween(2,500);
        Ensure.That(lastName).HasLengthBetween(2, 5000);
        
        FirstName = firstName;
        LastName = lastName;
    }
}