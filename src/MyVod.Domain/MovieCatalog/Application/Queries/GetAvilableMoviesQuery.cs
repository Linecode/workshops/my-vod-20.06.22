using MediatR;
using MyVod.Domain.MovieCatalog.Domain;
using MyVod.Domain.MovieCatalog.Domain.Specifications;
using MyVod.Domain.MovieCatalog.Infrastructure;

namespace MyVod.Domain.MovieCatalog.Application.Queries;

public record GetAvilableMoviesQuery(string StartsFrom) : IRequest<IEnumerable<Movie>>;

internal class GetAviableMoviesHandler : IRequestHandler<GetAvilableMoviesQuery, IEnumerable<Movie>>
{
    private readonly IMoviesRepository _repository;

    public GetAviableMoviesHandler(IMoviesRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<IEnumerable<Movie>> Handle(GetAvilableMoviesQuery request, CancellationToken cancellationToken)
    {
        var spec = new PublishedMovieSpecification().And(new TitleStartFromMovieSpecification(request.StartsFrom));

        var result = await _repository.Get(spec);

        return result!;
    }
}