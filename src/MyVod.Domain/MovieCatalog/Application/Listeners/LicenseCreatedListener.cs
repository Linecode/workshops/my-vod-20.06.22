using MediatR;
using MyVod.Domain.CannonicalModel.Events;
using MyVod.Domain.MovieCatalog.Application.Commands;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MovieCatalog.Application.Listeners;

public class LicenseCreatedListener : INotificationHandler<LicenseCreated>
{
    private readonly ISender _sender;

    public LicenseCreatedListener(ISender sender)
    {
        _sender = sender;
    }
    
    public async Task Handle(LicenseCreated notification, CancellationToken cancellationToken)
    {
        // await _sender.Send(new PublishMovieCommand(new MovieId(notification.MovieId)), cancellationToken);
    }
}