using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.CannonicalModel.Events;

namespace MyVod.Domain.Legal.Domain;

public sealed class License : Entity<Guid>, AggregateRoot<Guid>
{
    public DateRange DateRange { get; private set; }

    public RegionIdentifier RegionIdentifier { get; private set; }

    public LicenseStatus Status { get; private set; } = LicenseStatus.NotActive;

    public Guid MovieId { get; private set; }

    public License(DateRange dateRange, RegionIdentifier regionIdentifier, Guid movieId)
    {
        DateRange = dateRange;
        RegionIdentifier = regionIdentifier;
        MovieId = movieId;

        Id = Guid.NewGuid();
        
        Raise(new LicenseCreated(Id, movieId, dateRange, regionIdentifier));

        if (dateRange.IsWithinRange(DateTime.UtcNow))
        {
            Activate();
        }
    }

    public void Activate()
    {
        Status = LicenseStatus.Active;
        Raise(new LicenseActivated(Id, MovieId, DateRange, RegionIdentifier));
    }

    public void DeActivate()
    {
        Status = LicenseStatus.NotActive;
        Raise(new LicenseDeActivated(MovieId));
    }

    public enum LicenseStatus
    {
        Active,
        NotActive
    }
}