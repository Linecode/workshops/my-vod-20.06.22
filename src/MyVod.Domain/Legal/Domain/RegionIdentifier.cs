using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Legal.Domain;

public class RegionIdentifier : ValueObject<RegionIdentifier>
{
    public ushort Value { get; private set; } = ushort.MinValue;

    [Obsolete("Only for EF", true)]
    private RegionIdentifier()
    {
    }
    
    public RegionIdentifier(ushort id)
    {
        Value = id;
    }

    public override string ToString()
        => Value.ToString().PadLeft(3, '0');

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }

    public static readonly RegionIdentifier Poland = new(616);
    // ReSharper disable once InconsistentNaming
    public static readonly RegionIdentifier USA = new(840);
    public static readonly RegionIdentifier Missing = new(000);
    public static readonly RegionIdentifier All = new(999);
}