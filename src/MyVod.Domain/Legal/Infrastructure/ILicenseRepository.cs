using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Legal.Domain;

namespace MyVod.Domain.Legal.Infrastructure;

public interface ILicenseRepository : IRepository
{
    Task Add(License license);
    Task<License> Get(Guid id);
}