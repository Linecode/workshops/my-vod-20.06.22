using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.CannonicalModel.PublishedLanguage;

public class PaymentIntend : ValueObject<PaymentIntend>
{
    public string Id { get; private set; }
    public Uri RedirectUrl { get; private set; }

    public PaymentIntend(string id, Uri redirectUrl)
    {
        Id = id;
        RedirectUrl = redirectUrl;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Id;
    }
}
