using MyVod.Domain.CannonicalModel.PublishedLanguage;

namespace MyVod.Domain.Sales.Application.Ports;

public interface IPaymentGateway
{
    Task<PaymentIntend> PayOrder(string name, long price, string currency, Guid orderId);
}