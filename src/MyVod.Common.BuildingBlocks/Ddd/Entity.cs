namespace MyVod.Common.BuildingBlocks.Ddd;

public abstract class Entity<TIdentifier> : IEntity
{
    public virtual TIdentifier Id { get; protected set; }
    protected virtual object Actual => this;
        
    private List<object> _domainEvents;
    public IReadOnlyList<object> DomainEvents => _domainEvents; 

    public void Raise(object eventItem)
    {
        _domainEvents = _domainEvents ?? new List<object>();
        _domainEvents.Add(eventItem);
    }

    public void RemoveDomainEvent(object eventItem)
        => _domainEvents?.Remove(eventItem);

    public void ClearDomainEvents()
        => _domainEvents?.Clear();

    public override bool Equals(object obj)
    {
        if (!(obj is Entity<TIdentifier> other))
            return false;

        if (ReferenceEquals(this, other))
            return true;

        if (Id.Equals(default(TIdentifier)) || other.Id.Equals(default(TIdentifier)))
            return false;

        return Id.Equals(other.Id);
    }

    public static bool operator ==(Entity<TIdentifier> a, Entity<TIdentifier> b)
    {
        if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            return true;

        if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(Entity<TIdentifier> a, Entity<TIdentifier> b)
        => !(a == b);

    public override int GetHashCode()
        =>  (Actual.GetType() + Id.ToString()).GetHashCode();
}