using MyVod.Common.BuildingBlocks.Ddd;
using Newtonsoft.Json;

namespace MyVod.Common.BuildingBlocks.EFCore;

public class AggregateBase : AggregateRoot<Guid>
{
    public Guid Id { get; protected set; }
    
    // For protecting the state, i.e. conflict prevention
    // The setter is only public for setting up test conditions
    public long Version { get; set; }
    
    [JsonIgnore]
    private readonly List<object> _uncommittedEvents = new();

    public IEnumerable<object> GetUncommittedEvents()
        => _uncommittedEvents;

    public void ClearUncommittedEvents()
        => _uncommittedEvents.Clear();

    protected void AddUncommittedEvent(object @event)
        => _uncommittedEvents.Add(@event);
}